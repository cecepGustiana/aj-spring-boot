package id.co.cecep.springboot.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.cecep.springboot.model.BaseModel;
import id.co.cecep.springboot.model.Topic;
import id.co.cecep.springboot.model.response.ResponseTopic;
import id.co.cecep.springboot.service.TopicService;

@RestController
@RequestMapping("/topics")
public class TopicController {

	@Autowired
	private TopicService topicService;
	
	Logger logger = LoggerFactory.getLogger(TopicController.class);
	
	@GetMapping
	public ResponseTopic.MultipleResult listTopic(){
		
		ResponseTopic.MultipleResult ret;
		List<Topic> listTopics = topicService.getAllTopics();
		if(listTopics.isEmpty()) {
			ret = new ResponseTopic.MultipleResult("404","Data is Empty");
		}else {
			ret = new ResponseTopic.MultipleResult("200","Data Found");
			ret.setResult(listTopics);
		}
		
		return ret;
	}
	
	@GetMapping("/{id}")
	public ResponseTopic.SingleResult getTopicById(@PathVariable("id") String id) {
		
		ResponseTopic.SingleResult ret;
		Topic retTopic = topicService.getTopicById(id);
		if(retTopic == null) {
			ret = new ResponseTopic.SingleResult("404","Data not Found");
		}else {
			ret = new ResponseTopic.SingleResult("200","Data Found");
			ret.setResult(retTopic);
		}
		return ret;
	}
	
	@PostMapping
	public BaseModel addTopic(@RequestBody Topic topic) {
		return topicService.addTopic(topic) == 1 ? new BaseModel("200","Success Insert") : new BaseModel("400","Failure");
	}
	
	@PutMapping("/{id}")
	public BaseModel editTopic(@RequestBody Topic topic, @PathVariable("id") String id) {
		int retStatus = topicService.editTopic(topic, id);
		BaseModel retBaseModel;
		if(retStatus == -1) {
			retBaseModel = new BaseModel("404","Id not Found");
		}else if(retStatus == 0) {
			retBaseModel = new BaseModel("500","Something went Wrong");
		}else {
			retBaseModel = new BaseModel("200","Success Update");
		}
		return retBaseModel;
	}
	
	@DeleteMapping("/{id}")
	public BaseModel deleteTopic(@PathVariable("id") String id) {
		int retStatus = topicService.removeTopic(id);
		BaseModel retBaseModel;
		if(retStatus == -1) {
			retBaseModel = new BaseModel("404","Id not Found");
		}else if(retStatus == 0) {
			retBaseModel = new BaseModel("500","Something went Wrong");
		}else {
			retBaseModel = new BaseModel("200","Success Delete");
		}
		return retBaseModel;
	}
}
