package id.co.cecep.springboot.model.response;

import java.util.List;

import id.co.cecep.springboot.model.BaseModel;
import id.co.cecep.springboot.model.Topic;

public class ResponseTopic{

	public static class SingleResult extends BaseModel{
		private Topic result;
		
		public SingleResult() {
		}

		public SingleResult(String status, String message) {
			super(status, message);
		}

		public Topic getResult() {
			return result;
		}

		public void setResult(Topic result) {
			this.result = result;
		}
		
	}
	
	public static class MultipleResult extends BaseModel{
		private List<Topic> result;
		
		public MultipleResult() {
		}

		public MultipleResult(String status, String message) {
			super(status, message);
		}

		public List<Topic> getResult() {
			return result;
		}

		public void setResult(List<Topic> result) {
			this.result = result;
		}
	}

	
}
