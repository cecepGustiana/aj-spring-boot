package id.co.cecep.springboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.cecep.springboot.model.Topic;

public interface TopicRepository extends JpaRepository<Topic, String>{

	public List<Topic> findAllByOrderByIdAsc();
	
}
