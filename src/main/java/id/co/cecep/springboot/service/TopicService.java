package id.co.cecep.springboot.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.cecep.springboot.model.Topic;
import id.co.cecep.springboot.repository.TopicRepository;

@Service
public class TopicService {

	Logger logger = LoggerFactory.getLogger(TopicService.class);
	
	@Autowired
	private TopicRepository topicRepository;
	
	public List<Topic> getAllTopics(){
		logger.debug("Find All Topics By Order By Id Asc");
		return topicRepository.findAllByOrderByIdAsc();
	}
	
	public Topic getTopicById(final String id) {
		logger.debug("Find Topic By Id");
		logger.info("ID : "+id);
		Optional<Topic> optional = topicRepository.findById(id);
		if(optional.isPresent()) {
			return optional.get();
		}
		return null;
	}
	
	public int addTopic(Topic topic){
		logger.debug("Save Topic");
		logger.info(topic.toString());
		Topic ret = topicRepository.save(topic);
		if(ret.getId() == null) {
			return 0;
		}
		return 1;
	}
	
	public int editTopic(Topic topic, String id){
		
		if(getTopicById(id) == null) {
			return -1;
		}
		
		topic.setId(id);
		logger.debug("Update Topic By Id");
		logger.info(topic.toString());
		Topic ret = topicRepository.save(topic);
		if(ret.getId() == null) {
			return 0;
		}
		return 1;
	}
	
	public int removeTopic(String id) {
		if(getTopicById(id) == null) {
			return -1;
		}
		logger.debug("Delete Topic By Id");
		topicRepository.deleteById(id);
		return 1;
	}
	
}
